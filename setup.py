from setuptools import setup, find_packages

install_requires = [
    "attrs",
    "cached-property",
    "click",
    "python-telegram-bot",
    "python-telegram-logger",
    "pytz",
    "PyYAML",
    "rtorrent-python",
]

setup(
    name="purger",
    version="0.1",
    packages=find_packages("src"),
    package_dir={"": "src"},
    scripts=[],
    install_requires=install_requires,
    include_package_data=True,
    entry_points={
        'console_scripts': ['purger=purger.cli:cli'],
    }
)
