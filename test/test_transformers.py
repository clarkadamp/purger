import re

import pytest

from purger.transformers import tracker_transformer


@pytest.fixture
def transformer_mapping():
    return {
        "torrent_leech": [
            re.compile(r".*tleechreload.*")
        ]
    }


@pytest.mark.parametrize("url, expected_type", [
    ("https://tracker.tleechreload.org/a/something/announce", "torrent_leech"),
    ("https://tracker.someother.org/a/something/announce", "default"),
])
def test_tracker_transformer(transformer_mapping, url, expected_type):
    assert tracker_transformer(transformer_mapping)(url) == expected_type
