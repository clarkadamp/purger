import os
from pathlib import Path

import pytest
import yaml

from purger.config import Config
from purger.exceptions import FuckNoException
from purger.model.dto import Torrent, TorrentFile
from purger.remover import is_subdir_of, Remover


@pytest.fixture(autouse=True)
def filesystem(tmpdir):

    managed_directory = tmpdir.join("managed")
    managed_directory.mkdir()
    keep_file = managed_directory.join("keep")
    keep_file.write("")
    typical_torrent_dir = managed_directory.join("typical_torrent")
    typical_torrent_dir.mkdir()
    typical_torrent_dir.join("subfolder").mkdir()
    typical_torrent_dir.join("subfolder").join("a-file.tgz").write("some content")

    all_empty_dirs = managed_directory.join("all_empty_dirs")
    all_empty_dirs.mkdir()
    all_empty_dirs.join("child1").mkdir()
    all_empty_dirs.join("child1").join("sub_child1").mkdir()
    all_empty_dirs.join("child1").join("sub_child2").mkdir()
    all_empty_dirs.join("child2").mkdir()
    all_empty_dirs.join("child2").join("sub_child2").mkdir()

    non_empty_dirs = managed_directory.join("non_empty_dirs")
    non_empty_dirs.mkdir()
    non_empty_dirs.join("child").mkdir()
    sub_child1 = non_empty_dirs.join("child").join("sub_child1")
    sub_child2 = non_empty_dirs.join("child").join("sub_child2")
    sub_child1.mkdir()
    sub_child2.mkdir()
    sub_child2.join("a_file.txt").write("some content")

    non_managed_dir = tmpdir.join("non_managed")
    non_managed_dir.mkdir()
    non_managed_file = non_managed_dir.join("other_file")
    non_managed_file.write("some content")

    yield
    #  This should always exist after
    assert managed_directory.exists()
    assert keep_file.exists()
    assert non_managed_file.exists()


def test_pathlib_similar():
    assert Path("/tmp") == Path("/tmp/")


@pytest.fixture
def config_file(tmpdir):
    config_contents = {
        "managed_directories": [
            tmpdir.join("managed").strpath
        ]
    }
    config_file = tmpdir.join("config-file.yaml")
    config_file.write(yaml.safe_dump(data=config_contents))
    return config_file.strpath


@pytest.fixture
def click_context(config_file):
    class Context:
        parent = None
        params = {
            "config_file": config_file
        }

    return Context()


@pytest.fixture()
def config(click_context):
    return Config(click_context)


@pytest.fixture
def remover(config):
    return Remover(config.managed_directories)


@pytest.fixture
def torrent_file(tmpdir):
    return TorrentFile(
        path=Path(tmpdir.strpath) / "managed" / "typical_torrent" / "subfolder" / "a-file.tgz",
        completed=True
    )


@pytest.fixture
def torrent(tmpdir, torrent_file):
    def files(*_):
        return [
            torrent_file
        ]

    return Torrent(
        source_obj=None,
        unique_id=None,
        callback_files=files,
        callback_remove=None,
        callback_trackers=None,
        callback_health=None,
        name=None,
        base_directory=Path(tmpdir.join("managed").join("typical_torrent").strpath),
        state=None,
        completed=None,
        size=None,
        downloaded=None,
        uploaded=None,
        ratio=None,
        last_changed=None,
        status_message=None,
    )


@pytest.mark.parametrize("parent_path, child_path, expected_result", [
    ("/some/path", "/some/path/child", True),
    ("/some/path", "/some/other/path/child", False),
])
def test_is_subdir_of(parent_path, child_path, expected_result):
    assert is_subdir_of(Path(parent_path), Path(child_path)) == expected_result


def test_remove_files(tmpdir, remover, torrent, torrent_file):
    assert torrent_file.path.exists()
    remover.remove(torrent.base_directory, [f.path for f in torrent.files])
    assert not torrent_file.path.exists()
    assert not (Path(tmpdir.strpath) / "managed" / "typical_torrent" / "subfolder").exists()
    assert not (Path(tmpdir.strpath) / "managed" / "typical_torrent").exists()
    assert (Path(tmpdir.strpath) / "managed").exists()


@pytest.mark.parametrize("base_dir, file_path, expected_text", [
    ("managed/non_empty_dirs/child/sub_child2", None, "is not empty"),
    ("managed/non_empty_dirs/child/sub_child2/a_file.txt", None, "is not a directory"),
    ("non_managed", "non_managed/other_file", "is not within a managed directory"),
    ("managed/all_empty_dirs", "managed/non_empty_dirs/child/sub_child2/a_file.txt", "is not a descendent of")
])
def test_fuck_no(tmpdir, remover, base_dir,file_path, expected_text):
    with pytest.raises(FuckNoException) as exc:

        remover.remove(
            base_directory=Path(os.path.join(tmpdir.strpath, base_dir)),
            files=[Path(os.path.join(tmpdir.strpath, file_path))] if file_path is not None else []
        )
    assert expected_text in exc.value.message


@pytest.mark.skip(reason="coz")
def test_not_managed(tmpdir, remover, torrent):
    non_managed_path = Path(tmpdir.strpath) / "managed" / "non_managed"
    torrent.base_directory = non_managed_path
    with pytest.raises(FuckNoException) as exc:
        remover.remove(torrent.base_directory, [f.path for f in torrent.files])
    assert "is not within a managed directory" in exc.value.message
    assert non_managed_path.exists()


def test_remove_managed_directory(caplog, tmpdir, remover):
    managed_dir = Path(tmpdir.strpath) / "managed"
    to_be_deleted = Path(tmpdir.strpath) / "managed" / "typical_torrent" / "subfolder" / "a-file.tgz"
    remover.remove(managed_dir, [to_be_deleted])
    assert not to_be_deleted.exists()
    assert managed_dir.exists()
    assert "is a managed directory, skipping" in caplog.messages[-1]
