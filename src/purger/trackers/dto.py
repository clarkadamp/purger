import datetime
from enum import Enum

import attr


class MessageStatus(Enum):
    READ = "READ"
    UNREAD = "UNREAD"


@attr.s(auto_attribs=True, frozen=True)
class SeedingTorrent:
    name: str
    seeding_time: datetime.timedelta
    hit_and_run: bool
    local_id: str


@attr.s(auto_attribs=True, frozen=True)
class Message:
    title: str
    status: MessageStatus
