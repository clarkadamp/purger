import logging
import re
from typing import Iterable

from bs4 import BeautifulSoup

from purger.logging import TELEGRAM
from purger.trackers.dto import Message, MessageStatus
from purger.website import WebSite

logger = logging.getLogger(__name__)


class PeerJunkies(WebSite):
    RATIO_RE = re.compile(r"[Rr]atio:.*<font.*?>(?P<ratio>[\d.]+).*?</font>")

    def is_logged_in(self, content: BeautifulSoup) -> bool:
        if "Not logged in" in content.text:
            return False
        if "Click to Reset Password" in content.text:
            return False
        return True

    def __init__(self, username: str, password: str) -> None:
        super().__init__()
        self.username = username
        self.password = password

    def login(self):
        return self.session.post(
            'https://www.peerjunkies.com/takelogin.php',
            data={
                'username': self.username,
                'password': self.password,
            }
        )

    def _get_messages(self) -> Iterable[Message]:
        content = self.get_url("https://www.peerjunkies.com/messages.php?action=viewmailbox")
        messages_table = content.find("table", {"class": "msg_table"})
        for message_row in messages_table.find_all("tr")[1:]:  # Skips header row
            row_cells = message_row.find_all("td")
            yield Message(
                title=row_cells[1].find("a").text,
                status=MessageStatus.UNREAD if message_row.find("img", {"alt": "Unread"}) else MessageStatus.READ
            )

    def ratio(self) -> float:
        content = self.get_url("https://www.peerjunkies.com/index.php")
        div = content.find("div", {"class": "top-right"})
        match = self.RATIO_RE.search(str(div))
        if match:
            return float(match.group("ratio"))
        breakpoint()
        logger.log(TELEGRAM, f"Unable to parse ratio for {self.__class__.__name__}")
        return 0.0
