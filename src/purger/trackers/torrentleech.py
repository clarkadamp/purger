import logging
import re
from collections import namedtuple
from datetime import timedelta
from typing import List, Mapping

from bs4 import BeautifulSoup

from purger.exceptions import UnauthenticatedException, HitAndRunParsingException
from purger.logging import TELEGRAM
from purger.model.dto import HitAndRuns
from purger.trackers.dto import SeedingTorrent
from purger.website import WebSite, login_required

logger = logging.getLogger(__name__)

SeedingTime = namedtuple("SeedingTime", "days, hours, minutes, seconds")


class TorrentLeech(WebSite):
    """
    HnR table available at https://www.torrentleech.me/profile/<username>/hnr

    <html lang="en" class="html-lights-off">
    <!-- ... -->
    <body class="tl-lights-off">
    <!-- ... -->
    You currently have  <span class="har_total">10</span> HnR torrents. For more on how to remove torrents from this list check the bottom of this page.
    <div class="data-table-container" style="display:none;">
        <table id="harsTable" class="table table-striped table-hover data-table">
            <!-- ... -->
            <tbody>
            <tr>
                <td>Dexter New Blood S01E07 1080p WEB H264-PECULATE</td>
                <td>1.92 GB</td>
                <td>1.9 GB</td>
                <td>0.99</td>
                <td>9 days, 4 hrs, 49 mins, 57 secs</td>
                <!-- ... -->
            </tr>
            </tbody>
        </table>
    <!-- ... -->
    </div>
    </body>
    </html>
    """

    def is_logged_in(self, content: BeautifulSoup) -> bool:
        if content.find_all("div", {'class': 'access-page'}):
            return False
        return True

    def __init__(self, username: str, password: str, user_id: str) -> None:
        super().__init__()
        self.username = username
        self.password = password
        self.user_id = user_id

    def login(self):
        return self.session.post(
            'https://www.torrentleech.me/user/account/login/',
            data={
                'username': self.username,
                'password': self.password,
            }
        )

    def hit_and_runs(self) -> HitAndRuns:
        content = self.get_url(f"https://www.torrentleech.me/profile/{self.username}/hnr")
        return HitAndRuns(num_torrents=self._num_hit_and_runs(content), torrent_names=self._torrent_names(content))

    def _num_hit_and_runs(self, content: BeautifulSoup) -> int:
        har_span = content.find("span", {'class': 'har_total'})
        if not har_span:
            logger.info(f"Bad content: {content.text()}")
            raise HitAndRunParsingException(f"Unable check hit and runs on {self.__class__.__name__}")
        try:
            return int(har_span.get_text())
        except ValueError:
            logger.info(f"Bad content: {har_span}")
            raise HitAndRunParsingException(f'Unable to parse {har_span.get_text()} into int')

    def _torrent_names(self, content: BeautifulSoup) -> List[str]:
        table = content.find(id="harsTable")
        names = []
        for row in table.find_all("tr"):
            columns = row.find_all("td")
            if not columns:
                # <th> row
                continue
            names.append(f"{columns[0].text} ({columns[4].text})")
        return names

    def seeding(self) -> Mapping[str, SeedingTorrent]:
        content = self.get_url("https://www.torrentleech.me/profile/mrblack7834/seeding")
        table = content.find(id="profile-seedingTable")
        if not table:
            logger.log(TELEGRAM, "Unable to find seeding table")
            return {}
        tbody = table.find("tbody")
        torrents = {}
        for row in tbody.find_all("tr"):
            name = row.find(attrs={"class": "torrent_name"}).text.strip()
            torrent = SeedingTorrent(
                name=f"rTorrent.{name}",
                hit_and_run=as_boolean(row.find(attrs={"title": "Contained in HitnRUn list"}).text),
                seeding_time=seeding_time(row.find(attrs={"title": "Seeding Time"}).text),
                local_id=row.find(attrs={"class": "peerId"}).text.strip()
            )
            torrents[torrent.local_id] = torrent
        return torrents


def as_boolean(text: str) -> bool:
    return {
        "yes": True,
        "no": False
    }[text.strip().lower()]


def seeding_time(text: str) -> timedelta:
    time_re = re.compile(
        r"^(?:(?P<days>\d+) days?(?:, )?)?(?:(?P<hours>\d+) hrs?(?:, )?)?(?:(?P<minutes>\d+) mins?(?:, )?)?(?:(?P<seconds>\d+) secs?)?$"
    )
    result = time_re.match(text.strip().lower())
    days = result.group("days")
    hours = result.group("hours")
    minutes = result.group("minutes")
    seconds = result.group("seconds")
    return timedelta(
        days=int(days) if days else 0,
        hours=int(hours) if hours else 0,
        minutes=int(minutes) if minutes else 0,
        seconds=int(seconds) if seconds else 0
    )