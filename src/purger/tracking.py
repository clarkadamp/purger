import datetime
import gzip
import json
import logging
import os
import time
from typing import Any, Mapping, Optional, Tuple

from pytz import UTC

from purger.config import Config
from purger.transformers import activity_delta_ratio

logger = logging.getLogger(__name__)


class ActivityTracker:
    DEFAULT_DATA = {
        "version": "0",
        "torrents": {},
    }

    def __init__(self, config: Config, mutates: bool = False) -> None:
        self.config = config
        self.now = time.time()
        self.mutates = mutates
        self.activity_data = self.DEFAULT_DATA.copy()

    def purge_stale_data(self):
        self._purge_non_existent_torrents()

    def completed_time(self, torrent_id: str) -> Optional[datetime.datetime]:
        if torrent_id not in self.activity_data['torrents']:
            return None
        for record in self.activity_data['torrents'][torrent_id]['activity']:
            if record['completed']:
                return datetime.datetime.fromtimestamp(record['timestamp'])
        return None

    def data_points_per_day(self, torrent_id: str) -> Mapping[int, int]:
        data_points = {}
        now = datetime.datetime.now()
        for record in self.activity_data['torrents'][torrent_id]['activity']:
            offset = now - datetime.datetime.fromtimestamp(record['timestamp'])
            if offset.days not in data_points:
                data_points[offset.days] = 0
            data_points[offset.days] += 1

        return data_points

    def activity_for_time(
            self, torrent_id: str, from_timestamp: datetime.datetime, torrent_size: int
    ) -> Tuple[int, float]:
        if torrent_id not in self.activity_data['torrents']:
            return 0, 0.0
        upload = 0
        ratio = 0.0
        for delta_ratio in activity_delta_ratio(
                self.activity_data['torrents'][torrent_id]['activity'], torrent_size=torrent_size
        ):
            if from_timestamp < delta_ratio.timestamp:
                upload += delta_ratio.delta_upload
                ratio += delta_ratio.delta_ratio
        return upload, ratio

    def _purge_non_existent_torrents(self):
        active_torrents = set(torrent.unique_id for torrent in self.config.torrents)
        for torrent_id, activity_data in list(self.activity_data['torrents'].items()):
            if torrent_id not in active_torrents:
                if self.no_updates_for(activity_data['first_seen'], seconds=1):
                    logger.info(f"Removing inactive torrent {torrent_id}")
                    self.activity_data['torrents'].pop(torrent_id)

    def no_updates_for(self, timestamp, weeks=0, days=0, hours=0, minutes=0, seconds=0) -> bool:
        return self._compare_timestamps(timestamp, self.now) > datetime.timedelta(
            weeks=weeks, days=days, hours=hours, minutes=minutes, seconds=seconds
        )

    def __enter__(self) -> 'ActivityTracker':
        self._load_activity_data()
        self._update_torrent_activity()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.mutates:
            self._save_activity_data()

    def _load_activity_data(self) -> None:
        if os.path.exists(self.config.activity_data_location):
            self.activity_data = self._read_json(self.config.activity_data_location)

    def _update_torrent_activity(self):
        torrent_map = self.activity_data['torrents']
        for torrent in self.config.torrents:
            if torrent.unique_id not in torrent_map:
                logger.info(f"New torrent: {torrent.unique_id} - {torrent.name}")
                torrent_map[torrent.unique_id] = {
                    "first_seen": self.now,
                    "activity": []
                }
            torrent_data = torrent_map[torrent.unique_id]
            torrent_data["last_seen"] = self.now
            torrent_data["activity"].append(
                {
                    "completed": torrent.completed,
                    "downloaded": torrent.downloaded,
                    "uploaded": torrent.uploaded,
                    "timestamp": self.now,
                }
            )

    def _save_activity_data(self) -> None:
        self.activity_data['last_update'] = self.now
        self._write_json(self.config.activity_data_location, self.activity_data)

    @staticmethod
    def _write_json(location: str, contents: Mapping[str, Any]) -> None:
        os.makedirs(os.path.basename(location), exist_ok=True)
        with gzip.open(location, "wb") as fh:
            fh.write(json.dumps(contents, indent=4, sort_keys=True).encode("utf-8"))

    @staticmethod
    def _read_json(location: str) -> Mapping[str, Any]:
        with gzip.open(location, "rb") as fh:
            return json.load(fh)

    @staticmethod
    def _compare_timestamps(early: float, late: float) -> datetime.timedelta:
        date_early = datetime.datetime.fromtimestamp(early, tz=UTC)
        date_late = datetime.datetime.fromtimestamp(late, tz=UTC)
        return date_late - date_early
