import logging
import re
from datetime import timedelta, datetime
from pathlib import Path
from typing import Mapping, Any, List, Optional

import click.core
import yaml
from cached_property import cached_property

from purger.model.dto import Torrent, Tracker, Trackers, TrackerCredential
from purger.model.enums import Operator
from purger.remover import Remover
from purger.sources.base import TorrentSource
from purger.sources.rtorrent import RTorrent
from purger.transformers import tracker_transformer, transform_seeding_time
from purger.trackers.torrentleech import TorrentLeech
from purger.trackers.peerjunkies import PeerJunkies
from purger.website import WebSite

logger = logging.getLogger(__name__)


class Config:

    def __init__(self, click_context: click.core.Context):
        self._click_context = click_context
        self._torrents = []

    def flush_torrents(self):
        self._torrents.clear()

    @cached_property
    def cli_params(self) -> Mapping[str, Any]:
        context_stack = [self._click_context]
        current_context = self._click_context
        while current_context.parent is not None:
            context_stack.append(current_context.parent)
            current_context = current_context.parent
        params = {}
        for context in reversed(context_stack):
            params.update(context.params)
        return params

    @cached_property
    def torrent_sources(self) -> List[TorrentSource]:
        sources = []
        for source_name, source_details in self.config_file_contents['sources'].items():
            if source_name == "rtorrent":
                sources.append(
                    RTorrent(
                        username=source_details['username'],
                        password=source_details['password'],
                        host=source_details['host'],
                        port=source_details.get('port', 443),
                        endpoint=source_details.get("endpoint", "xmlrpc"),
                        tracker_transformer=tracker_transformer(self.trackers),
                        dry_run=self.dry_run,
                        remover=self.remover,
                    )
                )
        return sources

    @cached_property
    def manage_credentials(self) -> Mapping[str, str]:
        for source_name, source_details in self.config_file_contents['sources'].items():
            if source_name == "rtorrent":
                return {
                    "username": source_details['username'],
                    "password": source_details['password']
                }
        raise RuntimeError("No Website credentials found")

    @property
    def log_file(self) -> str:
        return self.cli_params['log_file']

    @cached_property
    def remover(self) -> Remover:
        return Remover(managed_directories=self.managed_directories)

    @property
    def torrents(self) -> List[Torrent]:
        if not self._torrents:
            logger.info("Refreshing torrent list")
            for source in self.torrent_sources:
                for torrent in source.torrents():
                    self._torrents.append(torrent)
        return self._torrents

    @cached_property
    def config_file_contents(self) -> Mapping[str, Any]:
        with open(self.cli_params['config_file']) as fh:
            return yaml.safe_load(fh.read())

    @cached_property
    def trackers(self) -> Trackers:

        trackers = []
        default_tracker = None
        for tracker_name, tracker_details in self.config_file_contents.get("trackers", {}).items():
            tracker = Tracker(
                name=tracker_name,
                min_seeding_time=transform_seeding_time(tracker_details.get("min_seed_time", "0d")),
                min_ratio=tracker_details.get("min_ratio", 0.0),
                operator=Operator(tracker_details.get("operator", "OR")),
                urls=tuple(re.compile(regex) for regex in tracker_details.get("tracker_urls", [])),
                account_ratio=tracker_details.get("account_ratio", 0.0),
                website=self._website(
                    tracker_name=tracker_name, login_details=self.tracker_credentials.get(tracker_name)
                )
            )
            if tracker_name == "default":
                default_tracker = tracker
            else:
                trackers.append(tracker)

        return Trackers(default=default_tracker, trackers=trackers)

    def _website(self, tracker_name: str, login_details: Optional[TrackerCredential]) -> Optional[WebSite]:
        trackers_cls = {
            "torrent_leach": TorrentLeech,
            "peerjunkies": PeerJunkies,
        }
        if tracker_name not in trackers_cls:
            return None
        if login_details is None:
            return None
        return trackers_cls[tracker_name](**login_details.credentials)

    @property
    def logging_level(self) -> int:
        if self.cli_params['debug']:
            return logging.DEBUG
        return logging.INFO

    @property
    def enable_notifications(self) -> bool:
        return self.cli_params['enable_notifications']

    @property
    def dry_run(self) -> bool:
        return not self.cli_params.get("fo_reals", False)

    @property
    def force(self) -> bool:
        return self.cli_params.get("force", False)

    @property
    def change_threshold(self) -> float:
        return 0.05

    @property
    def activity_data_location(self) -> str:
        location = self.cli_params['activity_data_location']
        if not location.endswith(".gz"):
            location = f"{location}.gz"
        return f"{location}"

    @cached_property
    def managed_directories(self) -> List[Path]:
        return [Path(directory) for directory in self.config_file_contents.get("managed_directories", [])]

    @property
    def inactivity_timestamp(self) -> datetime:
        return datetime.now() - self.inactivity_time

    @cached_property
    def inactivity_time(self) -> timedelta:
        return transform_seeding_time(self.config_file_contents.get('inactivity', {}).get('time', "7d"))

    @cached_property
    def inactivity_ratio(self) -> float:
        return self.config_file_contents.get('inactivity', {}).get('ratio', 0.1)

    @property
    def dump_reasons(self) -> Optional[click.utils.LazyFile]:
        return self.cli_params.get('dump_reasons')

    @property
    def torrend_id(self) -> Optional[str]:
        return self.cli_params.get('torrent_id', None)

    @property
    def limit(self) -> Optional[int]:
        return self.cli_params.get('limit', None)

    @cached_property
    def tracker_credentials(self) -> Mapping[str, TrackerCredential]:
        trackers = {}
        for tracker_name, tracker_details in self.config_file_contents.get("trackers", {}).items():
            credentials = tracker_details.get("credentials")
            if credentials:
                trackers[tracker_name] = TrackerCredential(
                    tracker_name=tracker_name,
                    credentials=credentials,
                )
        return trackers


class PathConfig:

    def __init__(self, path: Path, config_name: str = ".purger.yml") -> None:
        self.adam = "adam"
        self.path = path
        self.config_name = config_name

    @cached_property
    def _config(self) -> Mapping[str, Any]:
        discovered_configs = []
        config_name = self.config_name

        def descend_configs(path: Path) -> None:
            potential_config = path / config_name
            if potential_config.exists():
                discovered_configs.insert(0, yaml.safe_load(potential_config.read_text()))

            if path.parent.as_posix() != path.root:
                descend_configs(path.parent)

        descend_configs(self.path)

        merged_config = {}
        for config in discovered_configs:
            merged_config.update(config)
        return merged_config

    @property
    def managed(self) -> bool:
        return self._config.get("managed", False)

    @property
    def torrent_root(self) -> bool:
        return self._config.get("torrent_root", False)

    @property
    def unpacked(self) -> bool:
        return self._config.get("unpacked", False)
