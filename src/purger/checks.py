import functools
import logging
from datetime import datetime, timedelta

from purger.config import Config
from purger.logging import TELEGRAM
from purger.model.dto import Torrent
from purger.model.enums import Operator
from purger.trackers.torrentleech import TorrentLeech
from purger.tracking import ActivityTracker
from purger.transformers import sizeof_fmt

logger = logging.getLogger(__name__)


def check_completed(config: Config) -> None:
    """
    Only completed torrents can be removed
    """
    with ActivityTracker(config) as tracker:
        for torrent in config.torrents:
            torrent.completed_time = tracker.completed_time(torrent.unique_id)
            if torrent.completed_time is None:
                torrent.keep_reasons.append("Not completed")
            else:
                torrent.drop_reasons.append("Completed")


def check_hit_and_run(config: Config) -> None:
    """
    This check ensure that a torrent cannot be removed if it will cause a Hit and Run
    """
    now = datetime.now()
    for torrent in config.torrents:

        if not torrent.completed_time:
            continue

        seeding_time = now - torrent.completed_time
        for tracker in torrent.trackers:
            if tracker.operator == Operator.OR:
                if seeding_time >= tracker.min_seeding_time or torrent.ratio >= tracker.min_ratio:
                    torrent.drop_reasons.append(
                        f"{tracker.name}: seeding {seeding_time.days}d gte {tracker.min_seeding_time.days}d "
                        f"OR ratio {torrent.ratio:.2f} gte {tracker.min_ratio:.2f}"
                    )
                else:
                    torrent.keep_reasons.append(
                        f"{tracker.name}: seeding {seeding_time.days}d lt {tracker.min_seeding_time.days}d "
                        f"OR ratio {torrent.ratio:.2f} lt {tracker.min_ratio:.2f}"
                    )
            else:
                if seeding_time >= tracker.min_seeding_time and torrent.ratio >= tracker.min_ratio:
                    torrent.drop_reasons.append(
                        f"{tracker.name}: seeding {seeding_time.days}d gte {tracker.min_seeding_time.days}d "
                        f"AND ratio {torrent.ratio:.2f} gte {tracker.min_ratio:.2f}"
                    )
                else:
                    torrent.keep_reasons.append(
                        f"{tracker.name}: seeding {seeding_time.days}d lt {tracker.min_seeding_time.days}d "
                        f"AND ratio {torrent.ratio:.2f} lt {tracker.min_ratio:.2f}"
                    )


def check_consecutive_history(config: Config) -> None:
    """
    Make sure there is data points for the duration for inactivity history
    """
    with ActivityTracker(config) as tracker:
        for torrent in config.torrents:
            data_point_per_day = tracker.data_points_per_day(torrent_id=torrent.unique_id)
            filtered_points = {day: data_point_per_day.get(day, 0) for day in range(config.inactivity_time.days)}
            if 0 in filtered_points.values():
                days_with_zero_data_points = [day for day, data_points in filtered_points.items() if data_points == 0]
                days_str = ','.join((str(day + 1) for day in sorted(days_with_zero_data_points)))
                torrent.keep_reasons.append(f"No data points collected on the following previous days: {days_str}")
            else:
                torrent.drop_reasons.append(f"Sufficient activity data for {config.inactivity_time.days} days")


def check_inactivity(config: Config) -> None:
    """
    If a torrent has uploaded less than a threshold ratio for a configured period, it is a candidate for deletion
    """
    inactivity_timestamp = config.inactivity_timestamp
    with ActivityTracker(config) as tracker:
        for torrent in config.torrents:
            delta_upload, delta_ratio = tracker.activity_for_time(
                torrent_id=torrent.unique_id, from_timestamp=inactivity_timestamp, torrent_size=torrent.size
            )
            if delta_ratio > config.inactivity_ratio:
                torrent.keep_reasons.append(
                    f"Active Torrent: {delta_ratio:.2f} gt {config.inactivity_ratio:.2f} "
                    f"- {sizeof_fmt(delta_upload)}/{sizeof_fmt(torrent.size)} "
                    f"seeded in the last {config.inactivity_time.days}d "
                )
            else:
                torrent.drop_reasons.append(
                    f"Inactive Torrent: {delta_ratio:.2f} lt {config.inactivity_ratio:0.2f} "
                    f"- {sizeof_fmt(delta_upload)}/{sizeof_fmt(torrent.size)} "
                    f"seeded in the last {config.inactivity_time.days}d"
                )


def check_site_seeding_time(config: Config) -> None:
    seeding_torrents = {
        "torrent_leach": TorrentLeech(**config.tracker_credentials["torrent_leach"].credentials).seeding()
    }
    now = datetime.now()
    un_listed_torrents = []

    for torrent in config.torrents:

        if not torrent.completed_time:
            continue

        for tracker in torrent.trackers:
            triple_min_seeding_time = tracker.min_seeding_time + tracker.min_seeding_time + tracker.min_seeding_time
            if tracker.name == "torrent_leach":
                if torrent.local_id in seeding_torrents["torrent_leach"]:
                    tracker_seeding_time = seeding_torrents["torrent_leach"][torrent.local_id].seeding_time
                    if tracker_seeding_time <= tracker.min_seeding_time:
                        torrent.keep_reasons.append(f"Tracker seeding time: {tracker_seeding_time}")
                    else:
                        torrent.drop_reasons.append(f"Tracker seeding time: {tracker_seeding_time}")
                else:
                    un_listed_torrents.append(torrent)
                    completed_duration = now - torrent.completed_time
                    if completed_duration > triple_min_seeding_time:
                        torrent.drop_reasons.append(
                            f"Tracker seeding time: Not listed, but completed for more than "
                            f"{triple_min_seeding_time.days}d ({completed_duration.days}d)"
                        )
                    else:
                        torrent.keep_reasons.append(f"Tracker seeding time: Not listed")
            else:
                torrent.drop_reasons.append(f"Tracker seeding time: Not implemented")

    if un_listed_torrents:
        now = datetime.now()
        names = [
            f"{shorten_name(torrent)} {(now - torrent.completed_time).days}d" for torrent
            in sorted(un_listed_torrents, key=functools.partial(unlisted_sort_key, now=now))
        ]
        logger.log(logging.INFO, "{} Unlisted torrents:\n{}".format(len(names), "\n".join(names)))


def unlisted_sort_key(torrent: Torrent, now: datetime) -> timedelta:
    return now - torrent.completed_time


def shorten_name(torrent: Torrent, length: int = 60) -> str:
    if len(torrent.name) <= length:
        return torrent.name
    return f"{torrent.name[:length - 3]}..."


def check_account_ratio(config: Config) -> None:
    """
    This check ensure that a torrent cannot be removed if it will cause a Hit and Run
    """
    ratios = {}

    for torrent in config.torrents:

        for tracker in torrent.trackers:

            if tracker.website is None:
                torrent.drop_reasons.append(f"{tracker.name} does not show ratios")
                continue

            if tracker.account_ratio == 0.0:
                torrent.drop_reasons.append("No account ratio requirement")
                continue

            if tracker.name not in ratios:
                ratios[tracker.name] = tracker.website.ratio()

            if ratios[tracker.name] < tracker.account_ratio:
                torrent.keep_reasons.append(
                    f"Account ratio: {ratios[tracker.name]} gt {tracker.account_ratio}"
                )
            else:
                torrent.drop_reasons.append(
                    f"Account ratio: {ratios[tracker.name]} lt {tracker.account_ratio}"
                )
