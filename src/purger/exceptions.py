import sys


class PurgerException(Exception):
    exit_code = 1

    def __init__(self, message):
        self._message = message

    @property
    def message(self):
        return self._message


class TooMuchChangeException(PurgerException):
    """
    Thrown if the script will do too much at once.
    """

    def __init__(self, message, force_message=True):
        super(TooMuchChangeException, self).__init__(message)
        self.force_message = force_message

    @property
    def message(self):
        if self.force_message:
            return f"{self._message}. Use '{' '.join(sys.argv)} --force' to override"
        return self._message


class FuckNoException(PurgerException):
    """
    thrown if purger is asked to do something really terrible
    """


class UnauthenticatedException(PurgerException):
    """
    thrown if it a login is required
    """


class HitAndRunParsingException(PurgerException):
    """
    Thrown if we can't parse hit and run web scraping
    """