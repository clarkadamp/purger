import re
from datetime import datetime, timedelta
from pathlib import Path
from typing import Any, Callable, List, Iterator, Tuple, Optional, Mapping

import attr
from cached_property import cached_property

from purger.model.enums import TorrentState, Operator
from purger.website import WebSite

dto = attr.s(auto_attribs=True)


@attr.s(auto_attribs=True)
class Torrent:
    source_obj: Any
    unique_id: str
    local_id: str
    callback_files: Callable[['Torrent'], Iterator['TorrentFile']]
    callback_remove: Callable[['Torrent', str], None]
    callback_trackers: Callable[['Torrent'], Iterator['Tracker']]
    callback_health: Callable[['Torrent'], None]
    name: str
    base_directory: Path
    state: TorrentState
    completed: bool
    size: int
    downloaded: int
    uploaded: int
    ratio: float
    last_changed: datetime
    status_message: str
    completed_time: Optional[datetime] = attr.ib(default=None)
    keep_reasons: List[str] = attr.ib(factory=list)
    drop_reasons: List[str] = attr.ib(factory=list)

    @cached_property
    def files(self) -> List['TorrentFile']:
        return [f for f in self.callback_files(self)]

    @cached_property
    def trackers(self) -> List['Tracker']:
        return [t for t in self.callback_trackers(self)]

    def remove(self, message) -> None:
        self.callback_remove(self, message)

    def check_health(self) -> None:
        self.callback_health(self)

    @property
    def can_drop(self):
        return self.drop_reasons and not self.keep_reasons


@attr.s(auto_attribs=True)
class TorrentFile:
    path: Path
    completed: bool

    @property
    def exists(self):
        return self.path.exists()


@attr.s(auto_attribs=True, frozen=True)
class Tracker:
    name: str
    min_seeding_time: timedelta
    min_ratio: float
    account_ratio: float
    operator: Operator
    urls: Tuple[re.Pattern]
    website: Optional[WebSite]


@attr.s(auto_attribs=True, frozen=True)
class Trackers:
    default: Tracker
    trackers: List[Tracker]


@attr.s(auto_attribs=True, frozen=True)
class DeltaRatio:
    timestamp: datetime
    delta_ratio: float
    delta_upload: int


@attr.s(auto_attribs=True, frozen=True)
class TrackerCredential:
    tracker_name: str
    credentials: Mapping[str, str]


@attr.s(auto_attribs=True, frozen=True)
class HitAndRuns:
    torrent_names: Optional[List[str]] = attr.ib(default=None)
    num_torrents: Optional[int] = attr.ib(default=None)
