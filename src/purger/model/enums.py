from enum import Enum


class TorrentState(Enum):
    ACTIVE = "ACTIVE"
    PAUSED = "PAUSED"
    STOPPED = "STOPPED"
    UNKNOWN = "UNKNOWN"


class Operator(Enum):
    AND = "AND"
    OR = "OR"
