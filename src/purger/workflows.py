import logging
import xmlrpc
from pathlib import Path

import yaml

from purger.trackers.dto import MessageStatus
from purger.transformers import sizeof_fmt
from purger.checks import check_hit_and_run, check_completed, check_inactivity, check_consecutive_history, \
    check_site_seeding_time, check_account_ratio
from purger.config import Config
from purger.exceptions import TooMuchChangeException
from purger.logging import TELEGRAM
from purger.model.enums import TorrentState
from purger.orphans import collated_files, Manage
from purger.remover import remove_torrents
from purger.trackers.torrentleech import TorrentLeech
from purger.tracking import ActivityTracker

logger = logging.getLogger(__name__)


def health_check(config: Config) -> None:
    logger.log(TELEGRAM, "Starting health checks")
    count = 0
    for torrent in config.torrents:
        try:
            torrent.check_health()
        except xmlrpc.client.Fault as e:
            logger.warning(f"{torrent.name}: {e}")
        count += 1
    logger.log(TELEGRAM, f"Finished checking {count} torrents")


def update_activity_data(config: Config):
    logger.info("Updating activity data")
    with ActivityTracker(config, mutates=True) as tracker:
        tracker.purge_stale_data()


def remove_completed_deleted(config: Config) -> None:
    torrents = []
    torrent_count = 0

    for torrent in config.torrents:
        torrent_count += 1
        if torrent.state == TorrentState.UNKNOWN:
            if torrent.status_message == "Download registered as completed, but hash check returned unfinished chunks.":
                torrents.append(torrent)

    impacted_torrents = len(torrents) / torrent_count

    if impacted_torrents > config.change_threshold:
        impacted_pct = int(impacted_torrents * 100)
        threshold_pct = int(config.change_threshold * 100)
        if not config.force:
            raise TooMuchChangeException(
                f"This operation would have removed {len(torrents)} ({impacted_pct}%) of torrents "
                f"which is more than the threshold of {threshold_pct}%"
            )

    if torrents:
        logger.log(
            TELEGRAM, f"Removing {len(torrents)} torrent(s) as they are listed as completed, but have no local files"
        )
        for torrent in sorted(torrents, key=lambda t: t.name):
            torrent.remove(torrent.status_message)


def remove_finished_torrents(config: Config):
    for check in [
        check_completed,
        check_hit_and_run,
        check_consecutive_history,
        check_inactivity,
        check_site_seeding_time,
        check_account_ratio
    ]:
        check(config)

    if config.dump_reasons:
        data = []
        for torrent in config.torrents:
            data.append({
                "id": torrent.unique_id,
                "name": torrent.name,
                "would_drop": torrent.can_drop,
                "keep_reasons": torrent.keep_reasons,
                "drop_reasons": torrent.drop_reasons,
            })
        yaml.safe_dump(data, config.dump_reasons)
        return

    if config.torrend_id:
        drop_torrents = [t for t in config.torrents if config.torrend_id in (t.name, t.unique_id)]
        if not drop_torrents:
            raise RuntimeError(f"{config.torrend_id} not found")
        torrent = drop_torrents[0]
        if not torrent.can_drop:
            keep_reasons = '\n'.join(torrent.keep_reasons)
            raise RuntimeError(
                f"Can't drop {torrent.name} due to the following reasons:\n{keep_reasons}")
    else:
        drop_torrents = [t for t in config.torrents if t.can_drop]

    if config.limit:
        drop_torrents = drop_torrents[:config.limit]

    impacted_torrents = len(drop_torrents) / len(config.torrents)

    if impacted_torrents > config.change_threshold:
        impacted_pct = int(impacted_torrents * 100)
        threshold_pct = int(config.change_threshold * 100)
        if not config.force:
            raise TooMuchChangeException(
                f"This operation would have removed {len(drop_torrents)} ({impacted_pct}%) of torrents "
                f"which is more than the threshold of {threshold_pct}%",
                force_message=False
            )

    if drop_torrents:
        remove_torrents(drop_torrents)
    else:
        logger.info("No are ready to be dropped")


def detect_hit_and_runs(config: Config) -> None:
    trackers = {
        "torrent_leach": TorrentLeech
    }
    for tracker_name, tracker_cls in trackers.items():
        credentials = config.tracker_credentials[tracker_name]
        tracker = tracker_cls(**credentials.credentials)
        hit_and_runs = tracker.hit_and_runs()

        if hit_and_runs.torrent_names:
            logger.info(f"{len(hit_and_runs.torrent_names)} hit and runs on {tracker_cls.__name__}")
            message_parts = [f"⚠️️ {len(hit_and_runs.torrent_names)} hit and runs on {tracker_cls.__name__}️"]
            for torrent_name in hit_and_runs.torrent_names:
                message_parts.append(f"* {torrent_name}")
            logger.log(TELEGRAM, "\n".join(message_parts))
        elif hit_and_runs.num_torrents:
            logger.info(f"{hit_and_runs.num_torrents} hit and runs on {tracker_cls.__name__}")
            logger.log(TELEGRAM, f"⚠️️ {hit_and_runs.num_torrents} Hit and Runs on {tracker_cls.__name__}")


def cleanup_orphaned(config: Config) -> None:

    m = Manage(**config.manage_credentials)
    space = m.available_space()
    used_space = space['disk_space_used']
    total_space = space['disk_space_allotted']
    keep_below = int(total_space * .88)
    over_amount = used_space - keep_below
    if over_amount <= 0:
        logger.info(f"{sizeof_fmt(space['disk_space_used'])} < {sizeof_fmt(keep_below)}")
        return

    logger.log(
        TELEGRAM,
        f"Need to recover {sizeof_fmt(used_space - keep_below)} "
        f"to maintain a buffer of {sizeof_fmt(total_space - keep_below)}"
    )
    files = collated_files(config)
    candidate_drop = [f for f in files if not f.keep]
    drop = []
    for candidate in sorted(candidate_drop, key=lambda f: f.age, reverse=True):
        drop.append(candidate)
        if sum([f.size for f in drop]) > over_amount:
            break
    all_ophaned_size = sum(f.size for f in candidate_drop)
    dropping_size = sum(f.size for f in drop)
    logger.log(
        TELEGRAM,
        f"Removing {len(drop)} ({sizeof_fmt(dropping_size)}) "
        f"out of {len(candidate_drop)} ({sizeof_fmt(all_ophaned_size)}) orphaned files"
    )
    for f in drop:
        logger.log(TELEGRAM, f"Removing {f.path.as_posix()}: {sizeof_fmt(f.size)} ({f.age.days} days)")
        f.path.unlink(missing_ok=True)


def remove_empty_directories(path: Path, files):
    for child in path.iterdir():
        if child.is_dir():
            remove_empty_directories(child, files)
    try:
        path.rmdir()
        logger.info(f"Removed {path}")
    except OSError as e:
        logger.info(e)
        folder_files = [f.name for f in path.iterdir() if not f.is_dir()]
        files[str(path)] = folder_files


def check_messages(config: Config) -> None:
    for tracker in config.trackers.trackers:
        if tracker.website is None:
            continue
        unread_messages = [m for m in tracker.website.mail_messages() if m.status == MessageStatus.UNREAD]
        if not unread_messages:
            continue
        message_titles = "\n* ".join(m.title for m in unread_messages)
        logger.log(
            TELEGRAM,
            f"{tracker.website.__class__.__name__} has {len(unread_messages)} unread messages:\n* {message_titles}"
        )
