from datetime import datetime, timedelta
from pathlib import Path
from typing import Mapping, Iterable, Optional, List, Tuple
import logging
import attr
from cached_property import cached_property
from bs4 import BeautifulSoup
from purger.config import Config
from purger.exceptions import UnauthenticatedException
from purger.logging import TELEGRAM
from purger.model.dto import Torrent
from purger.website import WebSite, login_required

logger = logging.getLogger(__name__)


class TorrentFiles:

    def __init__(self, config: Config) -> None:
        self.config = config

    @cached_property
    def torrent_files(self) -> Mapping[str, 'FileSystemItem']:
        files = {}
        for torrent in self.config.torrents:
            for file in torrent.files:
                files[file.path.as_posix()] = FileSystemItem(path=file.path, torrent=torrent)
        return files

    @cached_property
    def torrent_directories(self) -> Mapping[str, 'FileSystemItem']:
        return {
            t.base_directory.as_posix(): FileSystemItem(path=t.base_directory, torrent=t) for t in self.config.torrents
        }


class FileSystemFiles:

    def __init__(self, config: Config) -> None:
        self.config = config

    def managed_files(self) -> Iterable['FileSystemItem']:
        for managed_directory in self.config.managed_directories:
            if not managed_directory.exists():
                logger.log(TELEGRAM, f"Managed directory {managed_directory.as_posix()} does not exist")
                continue
            for path in descend_path(managed_directory):
                yield FileSystemItem(path=path)


@attr.s(auto_attribs=True)
class FileSystemItem:
    path: Path
    torrent: Optional[Torrent] = attr.ib(default=None)
    days: int = attr.ib(default=30)

    @property
    def size(self):
        return self.path.stat().st_size

    @property
    def is_config(self) -> bool:
        return self.path.name == ".purger.yml"

    @property
    def keep(self) -> bool:
        if self.keep_reasons:
            # Any keep reason keeps it
            return True
        if not self.drop_reasons:
            # The lack of a drop reason keeps it
            return True
        return False

    @property
    def drop_reasons(self) -> List[str]:
        reasons = []
        if not self.torrent:
            reasons.append(f"Not part of an active torrent")
        return reasons

    @property
    def age(self) -> timedelta:
        now = datetime.now()
        return now - self.timestamp(now)

    def timestamp(self, now):
        stat = self.path.stat()
        ts = None
        for timestamp in [stat.st_ctime, stat.st_mtime, stat.st_atime]:
            _datetime = datetime.fromtimestamp(timestamp)
            if _datetime > now:
                continue
            if ts is None or _datetime > ts:
                ts = _datetime
        if ts is None:
            ts = now - timedelta(days=30)
        return ts

    @property
    def keep_reasons(self) -> List[str]:
        reasons = []
        if self.is_config:
            reasons.append("Configuration file")
        if self.path.is_dir():
            reasons.append("Directory")
        if self.torrent:
            reasons.append(f"Active torrent: {self.torrent.name}")
        if self.age < timedelta(days=7):
            reasons.append(f"Time on disk < 7 days: {self.age.days}d {int(self.age.seconds / 86400)}")

        return reasons


class Manage(WebSite):

    def is_logged_in(self, content: BeautifulSoup) -> bool:
        return True

    def __init__(self, username: str, password: str) -> None:
        super().__init__()
        self.username = username
        self.password = password

    def login(self):
        response = self.session.get('https://whatbox.ca/login')
        soup = BeautifulSoup(response.content.decode("utf-8"), features="html.parser")
        token = soup.find('input', {'name': 'csrf_token_ip'})['value']
        response = self.session.post(
            'https://whatbox.ca/login',
            data={
                'csrf_token_ip': token,
                'username': self.username,
                'password': self.password,
            }
        )
        if "csrf_token_ip" in response.content.decode("utf-8"):
            raise UnauthenticatedException(self.__class__.__name__)

    @login_required
    def available_space(self) -> Mapping[str, int]:
        response = self.session.get("https://whatbox.ca/manage/storage?server=Titan&")
        if "csrf_token_ip" in response.content.decode("utf-8"):
            raise UnauthenticatedException(self.__class__.__name__)
        return response.json()


def collated_files(config: Config) -> List[FileSystemItem]:
    """
    Take all discovered filesystem files from FileSystemFiles and attempt to correlate them with a Torrent:
    1. explicitly an file from the torrent itself (seeding)
    2. under the torrents base directory (unpacked)

    If neither of these, the file is orphaned, and orphaned files can be deleted
    """
    files = []
    torrent_files = TorrentFiles(config)
    for item in FileSystemFiles(config).managed_files():
        files.append(item)
        posix_path = item.path.as_posix()
        if posix_path in torrent_files.torrent_files:
            item.torrent = torrent_files.torrent_files[posix_path].torrent
            continue
        if item.path.parent in config.managed_directories:
            # Is in the root of a managed path, so they should be explicit if active
            continue
        for torrent_directory in torrent_files.torrent_directories.values():
            try:
                item.path.relative_to(torrent_directory.path)
            except ValueError:
                # item not under torrent_directory
                continue
            item.torrent = torrent_directory.torrent
            break

    return files


def descend_path(root: Path,  include_directories: bool = False) -> Iterable[Path]:
    for child in root.iterdir():
        if child.is_dir():
            if include_directories:
                yield child
            yield from descend_path(root=child, include_directories=include_directories)
        else:
            yield child
