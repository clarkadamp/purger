import sys

from purger.cli import cli

if __name__ == "__main__":
    cli(prog_name=sys.argv[0])
