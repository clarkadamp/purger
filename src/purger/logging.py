import logging
import time
from datetime import datetime, timedelta
from functools import wraps
from logging.handlers import RotatingFileHandler

import python_telegram_logger.handlers

TELEGRAM = logging.ERROR + 1
TEN_MEGABYTES = 10 * 1000 * 1000


def configure_logging(config):
    logging.addLevelName(TELEGRAM, "🤖 Purger Script")
    root_logger = logging.getLogger()
    root_logger.setLevel(config.logging_level)
    if config.log_file:
        root_logger.handlers.clear()
        log_file_handler = RotatingFileHandler(config.log_file, maxBytes=TEN_MEGABYTES, backupCount=4)
        log_file_handler.setLevel(config.logging_level)
        log_file_handler.setFormatter(
            logging.Formatter(
                fmt="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
            )
        )
        root_logger.addHandler(log_file_handler)

    if not config.enable_notifications:
        return

    if 'telegram' in config.config_file_contents.get('notifications', {}):
        telegram_settings = config.config_file_contents['notifications']['telegram']
        telegram_handler = python_telegram_logger.handlers.Handler(
            token=telegram_settings['token'],
            chat_ids=telegram_settings['chat_ids'],
        )
        telegram_handler.setLevel(TELEGRAM)
        root_logger.addHandler(telegram_handler)


def flush_telegram_logs(func):
    root_handler = logging.getLogger()

    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        for handler in root_handler.handlers:
            if isinstance(handler, python_telegram_logger.handlers.Handler):
                while handler.queue.unfinished_tasks:
                    time.sleep(0.1)
        return result

    return wrapper


class CoolDownLogger:
    """
    Don't annoy the feed with error messages
    """

    def __init__(self, cool_down_hours=24):
        self.cool_down_hours = cool_down_hours
        self.cache = {}
        self.logger = logging.getLogger(self.__class__.__name__)

    def log(self, message) -> None:
        if self._should_log(message):
            self.cache[message] = datetime.now()
            self.logger.log(TELEGRAM, message)
        else:
            self.logger.info(message)

    def _should_log(self, key) -> bool:
        if key not in self.cache:
            return True
        if self.cache[key] + timedelta(hours=self.cool_down_hours) < datetime.now():
            return True
        return False
