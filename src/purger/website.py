import abc
import logging
from typing import List, Iterable

from bs4 import BeautifulSoup
import requests

from purger.exceptions import UnauthenticatedException
from purger.trackers.dto import Message

logger = logging.getLogger(__name__)


def login_required(func):

    def wrapper(website: "WebSite", *args, **kwargs):
        try:
            return func(website, *args, **kwargs)
        except UnauthenticatedException:
            website.login()
            logger.info(f"Logged into {website.__class__.__name__}")
            return func(website, *args, **kwargs)

    return wrapper


class WebSite(abc.ABC):

    def __init__(self):
        self.session = requests.session()

    @login_required
    def get_url(self, url: str) -> BeautifulSoup:
        response = self.session.get(url)
        content = BeautifulSoup(response.content.decode("utf-8"), features="html.parser")
        if not self.is_logged_in(content):
            raise UnauthenticatedException(self.__class__.__name__)
        return content

    @abc.abstractmethod
    def login(self):
        """
        :return:
        """

    @abc.abstractmethod
    def is_logged_in(self, content: BeautifulSoup) -> bool:
        """
        """

    def mail_messages(self) -> List[Message]:
        return [message for message in self._get_messages()]

    def _get_messages(self) -> Iterable[Message]:
        return []

    def ratio(self) -> float:
        return 0.0
