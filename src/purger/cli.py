from textwrap import dedent

import click
import logging
import time
from functools import wraps

from purger import workflows
from purger.config import Config
from purger.exceptions import PurgerException, TooMuchChangeException, HitAndRunParsingException
from purger.logging import configure_logging, flush_telegram_logs, TELEGRAM, CoolDownLogger

logger = logging.getLogger(__name__)


@click.group()
@click.option("--debug", default=False, is_flag=True, flag_value=True)
@click.option("--enable-notifications", default=False, is_flag=True, flag_value=True)
@click.option("--log-file", default=None)
@click.option("--config-file", default="config.yaml")
@click.pass_context
@flush_telegram_logs
def cli(context: click.core.Context, *_, **__) -> None:
    configure_logging(Config(context))


def subcommand(name: str):
    def decorator(f):
        @wraps(f)
        @cli.command(name=name)
        @click.pass_context
        @flush_telegram_logs
        def wrapper(context: click.core.Context, *_, **__):
            try:
                return f(Config(context))
            except PurgerException as e:
                logger.log(TELEGRAM, f"{e.__class__.__name__}: {e.message}")
                time.sleep(10)
                raise click.Abort()
            except Exception:
                logger.exception("Something horrible happened")
                raise click.Abort()

        return wrapper

    return decorator


@subcommand(name="health-check")
def health_check(config: Config) -> None:
    workflows.health_check(config)


@click.option("--activity-data-location", required=True)
@subcommand(name="update-activity-data")
def update_activity_data(config: Config) -> None:
    workflows.update_activity_data(config)


@click.option("--fo-reals", default=False, is_flag=True, flag_value=True)
@click.option("--force", default=False, is_flag=True, flag_value=True)
@subcommand(name="remove-completed-deleted")
def remove_completed_delete(config: Config) -> None:
    workflows.remove_completed_deleted(config)


@click.option("--activity-data-location", required=True)
@click.option("--fo-reals", default=False, is_flag=True, flag_value=True)
@click.option("--force", default=False, is_flag=True, flag_value=True)
@click.option("--dump-reasons", type=click.File('w'))
@click.option("--torrent-id", default=None)
@click.option("--limit", default=None, type=int)
@subcommand(name="drop-inactive")
def drop_inactive(config: Config) -> None:
    workflows.remove_finished_torrents(config)


@click.option("--activity-data-location", required=True)
@click.option("--fo-reals", default=False, is_flag=True, flag_value=True)
@click.option("--repeat-every-x-hours", default=0, type=int)
@subcommand(name="maintenance")
def maintenance(config: Config) -> None:
    repeat_hours = config.cli_params.get("repeat_every_x_hours") * 60 * 60
    cool_down_logger = CoolDownLogger()
    while True:
        try:
            workflows.check_messages(config)
            workflows.detect_hit_and_runs(config)
            workflows.update_activity_data(config)
            workflows.remove_finished_torrents(config)
            workflows.cleanup_orphaned(config)
        except TooMuchChangeException as e:
            cool_down_logger.log(
                dedent(
                    f"""\
                    {e.message}
                    As a safety measure, this script will not remove more than the above threshold of torrents.  \
                    To override, run:
                    /home/franksmum69/scripts/purger drop-inactive --activity-data-location /home/franksmum69/.config/torrent-activity.json.gz --fo-reals --force
                    """
                )
            )
        except HitAndRunParsingException as e:
            cool_down_logger.log(e.message)
        except Exception as e:
            if not repeat_hours:
                raise
            logger.exception("Something went wrong while looping")
            cool_down_logger.log(str(e))
        if not repeat_hours:
            break
        logger.info(f"Sleeping for {config.cli_params['repeat_every_x_hours']} hours")
        time.sleep(repeat_hours)
        config.flush_torrents()


@subcommand(name="hit-and-runs")
def hit_and_runs(config: Config) -> None:
    workflows.detect_hit_and_runs(config)


@subcommand(name="remove-orphans")
def remove_orphans(config: Config) -> None:
    workflows.cleanup_orphaned(config)


@subcommand(name="validate-notifications")
def validate_notifications(_) -> None:
    logger.log(TELEGRAM, "If you can see this, it's working!")


@subcommand(name="check-messages")
def remove_orphans(config: Config) -> None:
    workflows.check_messages(config)


if __name__ == "__main__":
    cli()
