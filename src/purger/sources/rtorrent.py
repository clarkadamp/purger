import time
from datetime import datetime
from shutil import rmtree

from cached_property import cached_property
from pathlib import Path
from typing import Iterator, Callable, List, Set
import logging
import pytz

import rtorrent
import rtorrent.torrent

from purger.model.dto import Torrent, TorrentFile, Tracker
from purger.model.enums import TorrentState
from purger.remover import Remover
from purger.sources.base import TorrentSource

logger = logging.getLogger(__name__)


class RTorrent(TorrentSource):

    def __init__(
            self,
            username: str,
            password: str,
            host: str,
            tracker_transformer: Callable[[str], Tracker],
            port: int,
            endpoint: str,
            dry_run: bool,
            remover: Remover
    ):
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        self.endpoint = endpoint
        self.tracker_transformer = tracker_transformer
        self.dry_run = dry_run
        self.remover = remover

    def torrents(self) -> Iterator[Torrent]:
        for torrent in self.client.get_torrents():
            yield Torrent(
                source_obj=torrent,
                unique_id=f"rTorrent.{torrent.rpc_id}",
                local_id=torrent.get_local_id(),
                callback_remove=self.remove,
                callback_files=self.files,
                callback_trackers=self.trackers,
                callback_health=self.health_check,
                name=torrent.name,
                base_directory=Path(torrent.directory),
                completed=not torrent.incomplete,
                size=torrent.size_bytes,
                downloaded=torrent.down_total,
                uploaded=torrent.up_total,
                ratio=torrent.ratio,
                last_changed=datetime.fromtimestamp(torrent.state_changed, tz=pytz.UTC),
                state=self._transform_state(torrent),
                status_message=torrent.message,
            )

    @staticmethod
    def files(torrent: Torrent) -> Iterator[TorrentFile]:
        torrent.source_obj.get_files()
        for torrent_file in torrent.source_obj.files:
            yield TorrentFile(
                path=Path(torrent.source_obj.directory) / torrent_file.path,
                completed=torrent_file.completed_chunks == torrent_file.size_chunks,
            )

    def remove(self, torrent: Torrent, reason: str) -> None:
        prefix = "DRYRUN - " if self.dry_run else ""
        file_text = " and associated files" if torrent.base_directory.exists() else ""
        logger.info(f"{prefix}Removing {torrent.name}{file_text}: {reason}")
        if not self.dry_run:
            try:
                self.remover.remove(
                    base_directory=torrent.base_directory,
                    files=[file.path for file in torrent.files]
                )
            finally:
                logger.info(f"Removing {torrent.name} from rTorrent")
                torrent.source_obj.close()
                torrent.source_obj.erase()

    def trackers(self, torrent: Torrent) -> Set[Tracker]:
        torrent.source_obj.get_trackers()
        _trackers = set()
        for tracker in torrent.source_obj.trackers:
            _trackers.add(self.tracker_transformer(tracker.url))
        return _trackers

    @cached_property
    def client(self):
        return rtorrent.RTorrent(self.rpc_endpoint)

    @property
    def rpc_endpoint(self) -> str:
        return f"https://{self.username}:{self.password}@{self.host}:{self.port}/{self.endpoint}"

    @staticmethod
    def _transform_state(torrent: rtorrent.torrent.Torrent) -> TorrentState:
        if torrent.open is False and torrent.state == 0:
            return TorrentState.STOPPED
        if torrent.open is True and torrent.state == 1:
            if torrent.active:
                return TorrentState.ACTIVE
            return TorrentState.PAUSED
        return TorrentState.UNKNOWN

    @staticmethod
    def health_check(torrent: Torrent):
        logger.debug(f"Hash checking {torrent.name}")
        torrent.source_obj.check_hash()
        torrent.source_obj.update()
        while torrent.source_obj.hash_checking_queued or torrent.source_obj.hash_checking:
            logger.debug(f"Still checking {torrent.name}")
            time.sleep(0.5)
            torrent.source_obj.update()
        logger.debug(f"Done checking {torrent.name}")
