import abc
from typing import Iterator

from purger.model.dto import Torrent


class TorrentSource(abc.ABC):

    @abc.abstractmethod
    def torrents(self) -> Iterator[Torrent]:
        """
        iterate torrents
        """

    @abc.abstractmethod
    def remove(self, torrent: Torrent, reason: str) -> None:
        """
        Remove a torrent from this source
        """
