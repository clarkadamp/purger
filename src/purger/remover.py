import logging
from pathlib import Path
from typing import List

from purger.exceptions import FuckNoException
from purger.logging import TELEGRAM
from purger.model.dto import Torrent
from purger.transformers import sizeof_fmt

logger = logging.getLogger(__name__)


def remove_torrents(torrents: List[Torrent]) -> None:
    space = sum(t.size for t in torrents)
    logger.log(TELEGRAM, f"Removing {len(torrents)} torrent(s), recovering {sizeof_fmt(space)} ")
    for torrent in sorted(torrents, key=lambda t: t.name):
        try:

            drop_reasons = "\n".join([f"* {reason}" for reason in torrent.drop_reasons])
            logger.log(TELEGRAM, f"Removing {torrent.name} "
                             f"({sizeof_fmt(torrent.size)}, {len(torrent.files)} files in {torrent.base_directory})"
                             f":\n{drop_reasons}")
            torrent.remove(f"{len(torrent.drop_reasons)} reasons")
        except FuckNoException as e:
            logger.log(TELEGRAM, f"Problem removing {torrent.name} due to: {e.message}")


class Remover:
    """
    This is the only place where we will delete anything
    """

    def __init__(self, managed_directories: List[Path]):
        self.managed_directories = managed_directories

    def remove(self, base_directory: Path, files: List[Path], ) -> None:
        self._sanity_check(base_directory, files)
        self._remove_files(files)
        self._remove_extracted_files(base_directory)
        self._remove_empty_directories(base_directory)

    def _sanity_check(self, base_directory: Path, files: List[Path]) -> None:
        self._check_base_path_within_managed(base_directory)
        self._check_files_are_under_base_directory(base_directory, files)

    def _check_base_path_within_managed(self, base_directory: Path) -> None:
        for managed_directory in self.managed_directories:
            if is_subdir_of(managed_directory, base_directory):
                return
        raise FuckNoException(f"{base_directory} is not within a managed directory")

    @staticmethod
    def _check_files_are_under_base_directory(base_directory: Path, files: List[Path]) -> None:
        for file in files:
            if not is_subdir_of(base_directory, file):
                raise FuckNoException(f"{file} is not a descendent of {base_directory}")

    @staticmethod
    def _remove_files(files: List[Path]):
        for file in files:
            if file.exists() and file.is_file():
                logger.info(f"Removing {file}")
                file.unlink()

    def _remove_extracted_files(self, base_directory: Path):
        """
        Sometimes torrents are unpacked for further processing

        ONLY:
          * do anything if the base directory is not a managed
          * the files directly under the base directory
        """
        if base_directory in self.managed_directories:
            logger.info(f"{base_directory} is a managed directory, skipping removing extracted files")
            return

        for child in base_directory.iterdir():
            if not child.is_dir():
                logger.info(f"Removing extracted file: {child}")
                child.unlink()

    def _remove_empty_directories(self, base_directory: Path) -> None:
        if base_directory in self.managed_directories:
            logger.info(f"{base_directory} is a managed directory, skipping")
            return

        if not base_directory.exists() or not base_directory.is_dir():
            raise FuckNoException(f"{base_directory} is not a directory")

        children = list(base_directory.iterdir())
        if not children:
            logger.info(f"Removing empty directory {base_directory}")
            base_directory.rmdir()
            return

        logger.info(f"{base_directory} contains {len(children)}item(s), descending")
        for child in children:
            # Depth first search and delete
            if not child.is_dir():
                raise FuckNoException(f"{base_directory} is not empty, {child} exists")
            self._remove_empty_directories(child)

        # Directory should be empty at this point, so re-run
        self._remove_empty_directories(base_directory)


def is_subdir_of(parent: Path, child: Path) -> bool:
    if parent.parts == child.parts[:len(parent.parts)]:
        return True
    return False
