import re
from datetime import timedelta, datetime
from typing import Callable, List, Any, Mapping

from purger.model.dto import Tracker, Trackers, DeltaRatio

time_statement_re = re.compile(r"(?P<multiplier>\d+)(?P<dimension>\w)")
supported_dimensions = {
    "d": 60 * 60 * 24
}


def tracker_transformer(
        trackers: Trackers
) -> Callable[[str], Tracker]:
    def transformer(tracker_url: str) -> Tracker:
        for tracker in trackers.trackers:
            for url in tracker.urls:
                if url.search(tracker_url):
                    return tracker

        return trackers.default

    return transformer


def transform_seeding_time(time_statement: str) -> timedelta:
    match = time_statement_re.search(time_statement)
    assert match, f"{time_statement} doesn't match {time_statement_re.pattern}"
    factor = match.group("dimension")
    assert factor in supported_dimensions, \
        f"Only the following dimensions are supported: {' '.join(supported_dimensions)}"
    return timedelta(seconds=int(match.group("multiplier")) * supported_dimensions[factor])


def activity_delta_ratio(activity_data: List[Mapping[str, Any]], torrent_size: int) -> List[DeltaRatio]:
    data = []
    first_record = activity_data[0]
    last_uploaded = first_record['uploaded']
    for record in activity_data:
        timestamp = record['timestamp']
        uploaded = record['uploaded']
        delta_uploaded = uploaded - last_uploaded
        data.append(
            DeltaRatio(
                timestamp=datetime.fromtimestamp(timestamp),
                delta_ratio=delta_uploaded / torrent_size,
                delta_upload=delta_uploaded,
            )
        )
        last_uploaded = uploaded
    return data


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)